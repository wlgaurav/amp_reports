package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import communication.MailUtils;
import communication.SlackUtils;
import communication.SmsUtils;
import constants.Constants;
import db.DatabaseUtils;
import formatting.CsvFormatter;
import logging.Logger;

public class Main {

	private static JsonObject json;
	private static Map<String, JsonObject> ereportMap = new HashMap<String, JsonObject>();
	private static Map<String, JsonObject> sreportMap = new HashMap<String, JsonObject>();
	private static String basePathForFiles = "output/";
	private static String basePublicUrl = "http://localhost/";

	private static Map<String, String> smsReportsMap = new HashMap<String, String>();
	
	public static void main(String[] args) {

		try {

			if (args == null || args.length < 1) {
				Logger.error("configFilePath expected as first argument");
				return;
			}

			json = readConfigFile(args[0]);
			if (json == null) {
				Logger.error("config file path is wrong, could not be found!");
				return;
			}
			initialiseSettings(json.getAsJsonObject("settings"));
			generateReports(json.get("reports").getAsJsonArray());
			if (Config.isEmailActive) {
				emailReports(json.getAsJsonArray("emails"));
			}
			if (Config.isSmsActive) {
				smsReports(json.getAsJsonArray("sms"));
			}
			if (Config.isSlackActive) {
				slackReports(json.getAsJsonArray("slack_msgs"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static JsonObject readConfigFile(String filename) throws IOException {
		if (!new File(filename).exists()) {
			return null;
		}
		JsonParser parser = new JsonParser();
		Object o = parser.parse(new FileReader(filename));
		return (JsonObject) o;
	}

	private static void slackReports(JsonArray array) {

		if (ereportMap == null || ereportMap.isEmpty()) {
			Logger.debug("no slack reports to be posted");
			return;
		}

		for (JsonElement je : array) {
			try {
				JsonObject json = je.getAsJsonObject();
				String title = json.get("title").getAsString();
				String body = json.get("body").getAsString();
				StringBuilder sb = new StringBuilder(body);
				JsonArray reportsArray = json.getAsJsonArray("reports");
				int size = reportsArray.size();
				String[] fileNames = new String[size];
				String[] filePaths = new String[size];
				int count = 0;
				boolean isBodyMsgOn = false;
				for (JsonElement each : reportsArray) {
					String reportId = each.getAsString();
					if (!ereportMap.containsKey(reportId)) {
						break;
					}
					int method = getReportSendingMechanism(reportId);
					switch (method) {
					case 1:
						String fileName = getReportNameForReportId(reportId);
						String filePath = getPublicPathForReportId(reportId);
						fileNames[count] = fileName;
						filePaths[count] = filePath;
						count++;
						break;
					case 2:
						String content = getReportContent(reportId, false);
						sb.append(Constants.EOL);
						sb.append(content);
						sb.append(Constants.EOL);
						isBodyMsgOn = true;
						break;
					default:
						sb.append(Constants.EOL);
						sb.append(getPublicPathForReportId(reportId));
						isBodyMsgOn = true;
						break;
					}
				}
				if(filePaths.length>0){
					SlackUtils.postMsg(title, fileNames, filePaths);
				}
				if(isBodyMsgOn == true){
					SlackUtils.postMsg(title, sb.toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void smsReports(JsonArray array) {

		if (sreportMap == null || sreportMap.isEmpty()) {
			Logger.debug("no sms reports to be sent");
			return;
		}

		for (JsonElement je : array) {
			try {
				JsonObject json = je.getAsJsonObject();
				JsonArray toArray = json.getAsJsonArray("to");
				String[] to = new String[toArray.size()];
				int count = 0;
				for (JsonElement each : toArray) {
					to[count++] = each.getAsString();
				}
				String body = json.get("body").getAsString();
				StringBuilder sb = new StringBuilder(body + Constants.EOL + Constants.EOL);
				JsonArray reportsArray = json.getAsJsonArray("reports");
				for (JsonElement each : reportsArray) {
					String reportId = each.getAsString();
					sb.append(smsReportsMap.get(reportId));
					sb.append(Constants.EOL);
				}

				SmsUtils.sendSms(to, sb.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static void emailReports(JsonArray array) {

		if (ereportMap == null || ereportMap.isEmpty()) {
			Logger.debug("no email reports to be sent");
			return;
		}

		for (JsonElement je : array) {
			try {
				JsonObject json = je.getAsJsonObject();
				String from = json.get("from").getAsString();

				JsonArray toArray = json.getAsJsonArray("to");
				String[] to = new String[toArray.size()];
				int count = 0;
				for (JsonElement each : toArray) {
					to[count++] = each.getAsString();
				}

				String[] cc = null;
				JsonArray ccArray = json.getAsJsonArray("cc");
				if (ccArray != null) {
					cc = new String[ccArray.size()];
					count = 0;
					for (JsonElement each : ccArray) {
						cc[count++] = each.getAsString();
					}
				}
				String subject = json.get("subject").getAsString();
				String body = json.get("body").getAsString();
				StringBuilder sb = new StringBuilder(body);
				
				JsonArray reportsArray = json.getAsJsonArray("reports");
				int size = reportsArray.size();
				String[] filePaths = new String[size];
				count = 0;
				for (JsonElement each : reportsArray) {
					String reportId = each.getAsString();
					if (!ereportMap.containsKey(reportId)) {
						break;
					}
					int method = getReportSendingMechanism(reportId);
					switch (method) {
					case 1:
						String filePath = getFilePathForReportId(reportId);
						filePaths[count] = filePath;
						count++;
						break;
					case 2:
						String content = getReportContent(reportId, true);
						sb.append(Constants.EOL_HTML);
						sb.append(content);
						sb.append(Constants.EOL_HTML);
						Logger.debug("mail content: " + content);
						break;
					default:
						sb.append(Constants.EOL_HTML);
						sb.append(getPublicPathForReportId(reportId));
						sb.append(Constants.EOL_HTML);
						break;
					}
				}
				
				MailUtils.sendMail(from, to, cc, subject, sb.toString(), filePaths);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private static String getReportContent(String reportId, boolean isHtml) throws FileNotFoundException {
		StringBuilder sb = new StringBuilder();
		String name = getReportNameForReportId(reportId);
		String filePath = getFilePathForReportId(reportId);
		String content = isHtml ? CsvFormatter.toHtml(name, filePath) : CsvFormatter.toSlack(name, filePath);
		sb.append(content);
		return sb.toString();
	}

	private static String getReportNameForReportId(String reportId) {
		JsonObject reportJson = ereportMap.get(reportId);
		return reportJson.get("name").getAsString();
	}

	private static String getFileNameForReportId(String reportId) {
		JsonObject reportJson = ereportMap.get(reportId);
		JsonArray communicationArray = reportJson.get("communication").getAsJsonArray();
		for (JsonElement jcomm : communicationArray) {
			JsonObject jcommJson = (JsonObject) jcomm;
			final String filename = jcommJson.get("outputFileName").getAsString();
			return filename;
		}
		return null;
	}

	private static String getFilePathForReportId(String reportId) {
		return formFilePath(getFileNameForReportId(reportId));
	}
	
	private static String getPublicPathForReportId(String reportId){
		return formPublicPath(getFileNameForReportId(reportId));
	}

	private static int getReportSendingMechanism(String reportId) {
		JsonObject reportJson = ereportMap.get(reportId);
		JsonArray communicationArray = reportJson.get("communication").getAsJsonArray();
		for (JsonElement jcomm : communicationArray) {
			JsonObject jcommJson = (JsonObject) jcomm;
			if (jcommJson.get("isActive").getAsBoolean()) {
				String type = jcommJson.get("type").getAsString();
				if ("email".equalsIgnoreCase(type) || "slack".equalsIgnoreCase(type)) {
					try {
						JsonElement methodObject = jcommJson.get("method");
						if (methodObject != null) {
							int method = methodObject.getAsInt();
							return method;
						}else{
							return 0;
						}
					} catch (Exception e) {
						e.printStackTrace();
						return 0;
					}
				} else {
					return -1;
				}
			}
		}
		return -1;
	}

	private static void generateReports(JsonArray array) {
		for (JsonElement je : array) {
			try {
				JsonObject reportJson = (JsonObject) je;
				Logger.info("reportJson: " + reportJson);
				final String reportId = reportJson.get("id").getAsString();
				final String reportName = reportJson.get("name").getAsString();
				boolean isActive = reportJson.get("isActive").getAsBoolean();
				if (isActive) {
					JsonArray communicationArray = reportJson.get("communication").getAsJsonArray();
					if (communicationArray.size() > 0) {

						if (reportJson.get("prequeries") != null) {
							final JsonArray prequeries = reportJson.get("prequeries").getAsJsonArray();
							if (prequeries != null && prequeries.size() > 0) {
								String[] prequeryArray = new String[prequeries.size()];
								int count = 0;
								for (JsonElement each : prequeries) {
									prequeryArray[count++] = each.getAsString();
								}
								DatabaseUtils.executeWithoutResult(prequeryArray);
							}
						}

						for (JsonElement jcomm : communicationArray) {
							JsonObject jcommJson = (JsonObject) jcomm;
							if (jcommJson.get("isActive").getAsBoolean()) {
								String type = jcommJson.get("type").getAsString();

								if ("email".equalsIgnoreCase(type)) {
									ereportMap.put(reportId, reportJson);
									final String filename = jcommJson.get("outputFileName").getAsString();
									final String filepath = formFilePath(filename);
									final String query = reportJson.get("query").getAsString();
									DatabaseUtils.executeAndWriteFile(query, filepath);
								} else if ("sms".equalsIgnoreCase(type)) {
									sreportMap.put(reportId, reportJson);
									final JsonArray queries = reportJson.get("queries").getAsJsonArray();
									StringBuilder sb = new StringBuilder(reportName + Constants.EOL);
									for (JsonElement qe : queries) {
										JsonObject qeJson = (JsonObject) qe;
										String query = qeJson.get("query").getAsString();
										String msg = qeJson.get("msg").getAsString();
										String value = DatabaseUtils.executeWithoutHeaders(query);
										sb.append(msg.replaceAll("X", value));
									}
									smsReportsMap.put(reportId, sb.toString());
								} else if ("slack".equalsIgnoreCase(type)) {
									ereportMap.put(reportId, reportJson);
									final String filename = jcommJson.get("outputFileName").getAsString();
									final String filepath = formFilePath(filename);
									final String query = reportJson.get("query").getAsString();
									DatabaseUtils.executeAndWriteFile(query, filepath);
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static String formFilePath(String filename) {
		return basePathForFiles + filename;
	}
	
	private static String formPublicPath(String filename) {
		return basePublicUrl + filename;
	}

	private static void initialiseSettings(JsonObject json) {
		initialiseCommunicationSettings(json.getAsJsonObject("communication"));
		initialiseDatabaseSettings(json.getAsJsonObject("db"));
		if (Config.isEmailActive) {
			initialiseSmtpSettings(json.getAsJsonObject("smtp"));
		}
		if (Config.isSmsActive) {
			initialiseSmsSettings(json.getAsJsonObject("sms"));
		}
		if (Config.isSlackActive) {
			initialiseSlackSettings(json.getAsJsonObject("slack"));
		}
		initialiseOutputSettings(json.getAsJsonObject("output"));
	}

	private static void initialiseOutputSettings(JsonObject json) {
		basePathForFiles = json.get("basePath").getAsString();
		basePublicUrl = json.get("basePublicUrl").getAsString();
	}

	private static void initialiseDatabaseSettings(JsonObject json) {
		String host = json.get("host").getAsString();
		String port = json.get("port").getAsString();
		String username = json.get("username").getAsString();
		String password = json.get("password").getAsString();
		String db = json.get("database").getAsString();
		String driver = json.get("driver").getAsString();
		DatabaseUtils.isLogging = true;
		DatabaseUtils.init(host, port, username, password, db, driver);
		Logger.info("database settings initiated");
	}

	private static void initialiseCommunicationSettings(JsonObject json) {
		try {
			Config.isEmailActive = json.get("isEmailActive").getAsBoolean();
		} catch (Exception e) {
		}
		try {
			Config.isSmsActive = json.get("isSmsActive").getAsBoolean();
		} catch (Exception e) {
		}
		try {
			Config.isSlackActive = json.get("isSlackActive").getAsBoolean();
		} catch (Exception e) {
		}
		Logger.info("communication settings initiated");
	}

	private static void initialiseSmtpSettings(JsonObject json) {
		String host = json.get("host").getAsString();
		String port = json.get("port").getAsString();
		String username = json.get("username").getAsString();
		String password = json.get("password").getAsString();
		MailUtils.isLogging = true;
		MailUtils.init(host, port, username, password);
		Logger.info("mail settings initiated");
	}

	private static void initialiseSmsSettings(JsonObject json) {
		String host = json.get("host").getAsString();
		String mobileKey = json.get("mobileKey").getAsString();
		String msgKey = json.get("msgKey").getAsString();
		SmsUtils.isLogging = true;
		SmsUtils.init(host, mobileKey, msgKey);
		Logger.info("sms settings initiated");
	}

	private static void initialiseSlackSettings(JsonObject json) {
		String hook = json.get("hook").getAsString();
		SlackUtils.isLogging = true;
		SlackUtils.init(hook);
		Logger.info("slack settings initiated");
	}
}
